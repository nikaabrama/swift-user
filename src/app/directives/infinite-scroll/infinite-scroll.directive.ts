import { DOCUMENT } from '@angular/common';
import {
  AfterViewInit,
  Directive,
  ElementRef,
  EventEmitter,
  Inject,
  OnDestroy,
  Output,
} from '@angular/core';
import { fromEvent, Subscription, tap } from 'rxjs';

@Directive({
  selector: '[sfInfiniteScroll]',
})
export class InfiniteScrollDirective implements AfterViewInit {
  @Output() scrolled = new EventEmitter<any>();
  constructor(
    private element: ElementRef,
    @Inject(DOCUMENT) private document: Document
  ) {}

  ngAfterViewInit() {
    fromEvent(this.document, 'scroll')
      .pipe(
        tap(() => {
          if (
            window.innerHeight + window.scrollY >=
            this.document.body.offsetHeight
          ) {
            this.scrolled.emit();
          }
        })
      )
      .subscribe();
  }
}
