import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserDetailComponent } from './user-detail.component';
import { UserCardModule } from '../../../components/user-card/user-card.module';
import { InfiniteScrollModule } from '../../../directives/infinite-scroll/infinite-scroll.module';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [UserDetailComponent],
  imports: [CommonModule, UserCardModule, InfiniteScrollModule, RouterModule],
  exports: [UserDetailComponent],
})
export class UserDetailModule {}
