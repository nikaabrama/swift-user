import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { Pagination } from 'src/app/models/pagination.model';
import { User, UserDetail } from 'src/app/models/user.model';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.scss'],
})
export class UserDetailComponent implements OnInit, OnDestroy {
  private subscription: Subscription = new Subscription();
  user?: UserDetail;
  paginationData?: Pagination;
  friendList: User[] = [];
  breadcrumbs: { title: string; id: number }[] = [];
  constructor(
    private userService: UserService,
    private activatedroute: ActivatedRoute
  ) {}

  ngOnInit() {
    this.subscription.add(
      this.activatedroute.params.subscribe((params: any) => {
        if (!params.id) {
          return;
        }
        this.friendList = [];
        this.loadUser(params.id);
        this.loadFriends(params.id, 1, 20);
      })
    );
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  loadUser(id: number) {
    this.userService.getUser(id).subscribe((response: UserDetail) => {
      this.user = response;
      if (
        !this.breadcrumbs.find((breadcrumb) => breadcrumb.id === response.id)
      ) {
        const userTitle = ` ${response.prefix} ${response.name} ${response.lastName}`;
        this.breadcrumbs.push({ title: userTitle, id: response.id });
      }
    });
  }

  loadFriends(userId: number, page: number, limit: number) {
    this.userService.getUserFriends(userId, page, limit).subscribe(
      (response) => {
        this.friendList = this.friendList.concat(response.list);
        this.paginationData = response.pagination;
      },
      (error) => {}
    );
  }

  onScrollBottom() {
    if (!this.paginationData?.nextPage) {
      return;
    }
    this.loadFriends(this.user!.id, this.paginationData.nextPage, 20);
  }
}
