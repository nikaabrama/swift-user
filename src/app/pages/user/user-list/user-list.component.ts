import { Component, OnInit } from '@angular/core';
import { Pagination } from 'src/app/models/pagination.model';
import { User } from '../../../models/user.model';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss'],
})
export class UserListComponent implements OnInit {
  paginationData?: Pagination;
  userList: User[] = [];

  constructor(private userService: UserService) {}

  ngOnInit() {
    this.loadData(1, 20);
  }

  loadData(page: number, limit: number) {
    this.userService.getUserList(page, limit).subscribe(
      (response) => {
        this.userList = this.userList.concat(response.list);
        this.paginationData = response.pagination;
      },
      (error) => {}
    );
  }

  onScrollBottom() {
    if (!this.paginationData?.nextPage) {
      return;
    }
    this.loadData(this.paginationData.nextPage, 20);
  }
}
