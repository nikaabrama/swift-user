import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserListComponent } from './user-list.component';
import { UserCardModule } from '../../../components/user-card/user-card.module';
import { InfiniteScrollModule } from '../../../directives/infinite-scroll/infinite-scroll.module';

@NgModule({
  declarations: [UserListComponent],
  imports: [CommonModule, UserCardModule, InfiniteScrollModule],
  exports: [UserListComponent],
})
export class UserListModule {}
