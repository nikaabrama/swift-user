import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Pagination } from 'src/app/models/pagination.model';
import { environment } from 'src/environments/environment';
import { User, UserDetail } from '../../../models/user.model';

@Injectable()
export class UserService {
  private baseUrl = environment.baseUrl;
  constructor(private http: HttpClient) {}

  getUserList(
    page: number,
    size: number
  ): Observable<{ list: User[]; pagination: Pagination }> {
    return this.http.get<{ list: User[]; pagination: Pagination }>(
      `${this.baseUrl}/user/${page}/${size}`
    );
  }

  getUser(id: number): Observable<UserDetail> {
    return this.http.get<UserDetail>(`${this.baseUrl}/user/${id}`);
  }
  getUserFriends(
    id: number,
    page: number,
    size: number
  ): Observable<{ list: User[]; pagination: Pagination }> {
    return this.http.get<{ list: User[]; pagination: Pagination }>(
      `${this.baseUrl}/user/${id}/friends/${page}/${size}`
    );
  }
}
