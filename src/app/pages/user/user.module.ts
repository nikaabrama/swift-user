import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserDetailModule } from './user-detail/user-detail.module';
import { UserListModule } from './user-list/user-list.module';
import { UserRoutingModule } from './user-routing.module';
import { UserService } from './services/user.service';

@NgModule({
  declarations: [],
  imports: [CommonModule, UserRoutingModule, UserListModule, UserDetailModule],
  providers: [UserService],
})
export class UserModule {}
