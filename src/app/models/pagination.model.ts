export interface Pagination {
  previousPage?: number;
  current: number;
  nextPage: number;
  total: number;
  pageSize: number;
}
